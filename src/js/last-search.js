import LastSearchInjection from './last-search-injection.js'

class LastSearch {

    constructor(defaults) {

        if(!this.checkDefaults(defaults)) {
            return
        }

        this.defaults = {
            language:'',
            secretKey:'',
            api:'',
            sessionCookie:this.readCookie('GNARHO'),
            bookingUrl:''
        }

        Object.assign(this.defaults, defaults);
        this.init()
    }

    checkDefaults(defaults){

        // TODO: Delete when explorer 11 is deprecated
        if(!!window.MSInputMethodContext && !!document.documentMode) {
            // eslint-disable-next-line no-console
            console.info('Internet exporer 11 hasn\'t support')
            return false
        }

        // Check default object
        if(typeof defaults !== 'object') {
            // eslint-disable-next-line no-console
            console.info('Default params must be an object')
            return false
        }

        // Check if language is soported language
        if(!defaults.hasOwnProperty('language') ||
        defaults.language != 'ca' &&
        defaults.language != 'es' &&
        defaults.language != 'fr' &&
        defaults.language != 'it' &&
        defaults.language != 'de' &&
        defaults.language != 'nl' &&
        defaults.language != 'en' &&
        defaults.language != 'ru' &&
        defaults.language != 'nl') {
            // eslint-disable-next-line no-console
            console.info('Language '+defaults.language+' is not correct language, init with default language')
            defaults.language = 'es'
        }

        // Breack if has one error
        if(!defaults.hasOwnProperty('secretKey') || defaults.secretKey == ''){
            // eslint-disable-next-line no-console
            console.info('secretKey must be filled')
            return false
        }

        if(!defaults.hasOwnProperty('api')){
            // eslint-disable-next-line no-console
            console.info('api must be filled')
            return false
        }

        if(!defaults.hasOwnProperty('bookingUrl')){
            // eslint-disable-next-line no-console
            console.info('bookingUrl must be filled')
            return false
        }

        return true

    }

    readCookie(name) {

        let cookies = document.cookie.split(";")
        let value = ''

        cookies.forEach((cookie)=>{
            let cookieName = cookie.split("=")
            if(cookieName[0].trim() == name) {
                value = cookieName[1]
            }
        })

        return value

    }

    init(){
        new LastSearchInjection(this.defaults)
    }

}

export default LastSearch
