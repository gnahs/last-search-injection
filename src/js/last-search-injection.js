import Serialize from './serialize.js'

class LastSearchInjection {

    constructor(defaults) {

        this.defaults = defaults
        this.lastSearchParams = {}
        this.initSettings()
        this.initSelectors()
        this.initCrossWindowMessageListenners()
        this.create()
    }

    initSettings(){
        this.settings = {
            srcJs:`${this.defaults.api}webscripts/v2/rho-last-search-with-dependencies.min.js`,
            srcStyles:`${this.defaults.api}webscripts/v2/rho-last-search-styles.min.css`,
            //srcJs:`http://rho-last-search.locale/dist/rho-last-search-with-dependencies.min.js`,
            //srcStyles:`http://rho-last-search.locale/dist/rho-last-search-styles.min.css`,
            isMobile: document.body.clientWidth < 768
        }
    }

    initSelectors(){
        const iframeName = 'rho-iframe-last-search'
        this.selectors = {
            body: document.querySelector('body'),
            iFrame:null,
            iFrameName: iframeName,
            iFrameId: iframeName
        }
    }

    create(){
        this.injectIFrame().then((id)=>{
            this.selectors.iFrame = document.querySelector('#'+id)
            this.injectMetas()
            this.injectJs()
            this.injectCss()
            this.injectExtraCss()
        })
    }

    // INJECT IFRAME FUNCTIONS
    injectIFrame() {
        var iFrame = document.createElement("iframe")
        iFrame.setAttribute("name", this.selectors.iFrameName)
        iFrame.setAttribute("id", this.selectors.iFrameId)
        iFrame.setAttribute("width", "99%")
        iFrame.setAttribute("style", "height: 160px;")
        iFrame.setAttribute("frameborder", "0")
        iFrame.setAttribute("allowtransparency", "true")
        iFrame.setAttribute("scrolling", "no")
        this.selectors.body.appendChild(iFrame)

        return new Promise((resolve) => {

            window.setTimeout(() => {
                resolve(this.selectors.iFrameId)
            }, 1500)

            // iFrame.onload = function (evt) {
            //     // when the iframe finish loading,
            //     // resolve with the iframe's identifier.
            //     resolve(evt.target.getAttribute('id'))
            // }
        })
    }

    injectCss() {
        var styles = this.selectors.iFrame.contentWindow.document.createElement("link")
        styles.rel = "stylesheet"
        styles.href = this.settings.srcStyles
        this.selectors.iFrame.contentWindow.document.getElementsByTagName('head')[0].appendChild(styles)
    }

    injectCutomStyles(){
        var style = this.selectors.iFrame.contentWindow.document.createElement("style")
        let styles = this.cutomStyle(this.lastSearchParams)
        style.appendChild(this.selectors.iFrame.contentWindow.document.createTextNode(styles))
        this.selectors.iFrame.contentWindow.document.getElementsByTagName('head')[0].appendChild(style)
    }

    injectExtraCss(){
        var style = document.createElement("style")
        let styles = this.stylesheet()
        style.appendChild(document.createTextNode(styles))
        document.getElementsByTagName('head')[0].appendChild(style)
    }

    stylesheet(){
        return `#rho-iframe-last-search{
            position: fixed;
            top: 0;
            right: 0;
            z-index: 99999999;
        }`
    }

    cutomStyle(message){

        let style = ''

        if(typeof message.headerColor !== 'undefined'){
            style = style + `.ls-container .header{
                background-color:${message.headerColor};
            } `
        }

        if(typeof message.buttonColor !== 'undefined'){
            style = style + `.ls-container .book-now{
                background:${message.buttonColor};
            } `
        }

        if(typeof message.offerColor !== 'undefined'){
            style = style + `.ls-container .hotel-price{
                color:${message.offerColor};
            } `
        }

        return style

    }

    injectMetas(){
        var metas = this.selectors.iFrame.contentWindow.document.createElement("meta")
        metas.content = "width=device-width, initial-scale=1"
        metas.name = "viewport"

        var metasUtf = this.selectors.iFrame.contentWindow.document.createElement("meta")
        metasUtf.charset = "utf-8"

        this.selectors.iFrame.contentWindow.document.getElementsByTagName('head')[0].appendChild(metasUtf)
        this.selectors.iFrame.contentWindow.document.getElementsByTagName('head')[0].appendChild(metas)
    }

    injectJs(){
        let script = this.selectors.iFrame.contentWindow.document.createElement("script")
        script.type = "text/javascript"
        script.src = this.settings.srcJs
        this.selectors.iFrame.contentWindow.document.getElementsByTagName('body')[0].appendChild(script)

        script.onload = (function(){
            this.callLastSearch()
        }.bind(this))
    }

    // WHEN IFRAME IS INJECTED CALL LAST SEARCH API
    callLastSearch(){
        this.load((xhr)=>{

            this.lastSearchParams = JSON.parse(xhr.responseText)

            if(!Object.keys(this.lastSearchParams).length){
                this.deleteIFrame()
                return
            }

            this.lastSearchParams.isMobile = this.settings.isMobile
            this.lastSearchParams.language = this.defaults.language

            this.injectCutomStyles()
            this.launchLastSearch()


        })
    }

    deleteIFrame(){
        let element = this.selectors.iFrame
        element.parentNode.removeChild(element)
        delete this.selectors.iFrame
    }

    launchLastSearch(){
        let iFrame = this.selectors.iFrame.contentWindow.window
        let event = new CustomEvent('last-search:message', {detail: this.lastSearchParams})
        iFrame.dispatchEvent(event)
    }

    load(callback){

        var xhr = new XMLHttpRequest()
        xhr.open('POST', `${this.defaults.api}lastsearch`, true)
        xhr.onreadystatechange = function () {
            if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                callback(xhr)
            }
        }
        xhr.send(JSON.stringify(this.defaults))
    }

    // EVENTS
    receivedMessageFromLastSearch(ev){
        let recievedData = ev.detail
        if(recievedData === null || !recievedData.hasOwnProperty('action')) {
            return
        }

        if(recievedData.action === 'resize'){
            this.resize(recievedData.option)
        }

        if(recievedData.action === 'booking'){
            this.redirectToBooking()
        }

        if(recievedData.action === 'close'){
            this.removeIframe()
        }
    }

    redirectToBooking(){

        let bookingParams = {
            destination: this.lastSearchParams.destinationId,
            dates: {
                checkIn: this.lastSearchParams.checkIn,
                checkOut: this.lastSearchParams.checkOut
            },
            occupancy:this.lastSearchParams.occupancy
        }

        let params = new Serialize(bookingParams)
        window.location.href = this.defaults.bookingUrl+params.encode()
    }

    removeIframe(){
        sessionStorage.setItem("lastSearch", true)
    }

    resize(option) {

        let width = "100%"
        let height = 120
        let layoutWidth = window.outerWidth

        if (layoutWidth > 768) {
            width = '326px'
            height = '200px'
        }

        if (option == "grow") {
            this.selectors.iFrame.style.height = height
            this.selectors.iFrame.style.width = width
        }

        if (option == "adjust") {
            let adjustedHeight =  this.selectors.iFrame.contentWindow.document.getElementsByClassName("ls-container")[0].offsetHeight
            adjustedHeight = adjustedHeight + 40
            this.selectors.iFrame.style.height = adjustedHeight + "px"
        }

        if (option == "minimize") {
            this.selectors.iFrame.style.width = "60px"
            this.selectors.iFrame.style.height = "60px"
        }

        if (option == "shrink") {
            this.selectors.iFrame.style.width = "0px"
            this.selectors.iFrame.style.height = "0px"
        }

    }

    initCrossWindowMessageListenners(){
        window.addEventListener('last-search:message', this.receivedMessageFromLastSearch.bind(this), false )
    }
}

export default LastSearchInjection
